To start project localhost

git clone

npm install

npm run dev



Test task for React.JS

The goal of this task is to develop a web application with simple user interface utilizing React.JS/Redux stack that can create product record and save it.

1. Use ES6+.
2. Make a page containing a form for adding new product (don?t use data layer npm modules such as 'redux-form')
3. Form should have following fields:
Name - input field with validation: 4-8 characters, only numbers and letters allowed.
Color - checkbox group (red, green, blue). The product can have more than one color.
4. Make a page with simple datagrid to show existing products with possibility to delete the product from the list.

It will be plus to:

1. Save data on backend using node.js + express.js or koa.js
2. Use redux for state management
3. Have form and datagrid available under separate tabs

You can use any UI library like 'react-toolbox' or 'react-bootstrap'.
Do your best to make created components reusable in any other applications.

Upon completion please provide us a link to git repository with the complete test task.
