/**
 * Created by nataly on 20.11.17.
 */

const productModel = require('../models').Product;
const colorModel = require('../models').Color;
const getPayload = require('./payload');

class ProductController {

    async indexInclude(req, res, next){
        const products = await productModel.findAll( {
            include: [{
                model: colorModel,
                as: 'Colors',
            }],
        });
        if (products.length >= 0){
            res.json(products)
        } else next(404)

    }

    async create(req, res, next){
        const payload = getPayload(req);
        let product;

        try {
            product = await productModel.create({name: payload.name});
        }
        catch (err){
            next(err);
        }

        if (product){
            let errors = [];
            const colors = payload.colors.split(',');
            for (const index in colors) {
                let item = {'name': colors[index], 'ProductId': product.id}
                let color;
                try {
                    color = await colorModel.create(item);
                }
                catch (err){
                    next(err)
                }
                if (!color) errors.push('500');
            }
            if (errors.length > 0) next(500);
            res.status(201).json({
                message: 'success',
                data: product
            });
        } else (next(500))
    }

    async delete(req, res, next){
        const changes = await productModel.destroy({
            where: {
                id: req.params.id
            }
        });
        if(changes > 0) {
            res.sendStatus(204)
        } else {
            next(404)
        }
    }
}

module.exports = new ProductController();