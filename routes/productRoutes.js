const router = require('express').Router();
const productController = require('../controllers/ProductController');

router.route('/')
    .get(productController.indexInclude)
    .post(productController.create);

router.route('/:id')
    .delete(productController.delete);

module.exports = router;