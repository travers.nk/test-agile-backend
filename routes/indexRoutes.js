/**
 * Created by nataly on 20.11.17.
 */

const productRoutes = require('./productRoutes');
const router = require('express').Router();

router.use('/product', productRoutes);

module.exports = router;
