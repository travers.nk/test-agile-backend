'use strict';

module.exports = (sequelize, DataTypes) => {
    const classMethods = {
        associate: models => {
            models.Color.belongsTo(models.Product, {
                onDelete: "CASCADE",
                foreignKey: {
                    allowNull: false
                }
            });
        }
    };

    const model = {
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            },
            allowNull: false,
        }
    };

    return sequelize.define('Color', model, {classMethods});
};
