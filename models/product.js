'use strict';

module.exports = (sequelize, DataTypes) => {

    const classMethods = {
        associate: models => {
            models.Product.hasMany(models.Color);
        }
    };

    const model = {
        name: {
            type: DataTypes.STRING,
            unique: true,
            validate: {
                len: [4,8],
                isAlphanumeric: true,
                notEmpty: true,
            },
            allowNull: false,
        },
    };

    return sequelize.define('Product', model, {classMethods});
};